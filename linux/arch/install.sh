#!/usr/bin/env bash

# Installs standard dev environment software on Arch Linux
sudo pacman -Syu

sudo pacman -S --noconfirm vim go feh mc git tig links wget curl \
				docker libreoffice firefox chromium \
				i3lock xautolock

cp ./.Xresources ~/.Xresources
cp -R ./.config/awesome ~/.config/
