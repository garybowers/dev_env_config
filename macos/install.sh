#!/usr/bin/env bash


###### Install XCode Command Line Tools ######
xcode_cli_tools=`xcode-select -p`
[[ if -z $xcode_cli_tools ]]; then \
	echo "Installed"; \
fi 
xcode-select --install
