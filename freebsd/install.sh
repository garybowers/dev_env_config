#!/usr/bin/env sh

#Update portstree
portsnap auto

###################
# Install Software
###################

#Install latest pkg
pkg install -y pkgconf

#Set up FreeBSD System
pkg install -y dbus hal
sysrc -q dbus_enable=yes
sysrc -q hald_enable=yes

#Install build tools
pkg install -y m4 gettext libxml2 flex bison libxslt gcc llvm60 gmake cmake autoconf automake kbuild

#Install editors
pkg install -y vim

#Install source management
pkg install -y git tig

#Install general tools
pkg install -y htop mc tmux screen w3m

#Install xorg
pkg install -y xorg xterm firefox gimp libreoffice virtualbox-ose-additions xrdp awesome thunderbird
sysrc -q vboxguest_enable="YES"
sysrc -q vboxservice_enable="YES"
sysrc -q xrdp_enable="YES"
sysrc -q xrdp_sesman_enable="YES"
